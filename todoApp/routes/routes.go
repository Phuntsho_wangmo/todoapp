package routes

import (
	"log"
	"myapp/controller"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func InitialisedRoutes() {
	router := mux.NewRouter()

	// ---------------SIGN UP/ LOGIN-----------------//
	router.HandleFunc("/signup", controller.Signup).Methods("POST")
	router.HandleFunc("/signup/{email}", controller.UpdateUser).Methods("PUT")
	router.HandleFunc("/signup/{email}", controller.DeleteUsers).Methods("DELETE")
	router.HandleFunc("/signup/{email}", controller.GetUser).Methods("GET")
	router.HandleFunc("/admins", controller.GetAllUsers).Methods("GET")

	router.HandleFunc("/login", controller.Login).Methods("POST")
	router.HandleFunc("/logout", controller.Logout)




	//----------------CRUD TODO---------------------//

	router.HandleFunc("/todo", controller.AddTodoList).Methods("POST") //to post
	//get
	router.HandleFunc("/todo/{todoid}", controller.GetTodolist).Methods("GET") //to post
	// //update
	router.HandleFunc("/todo/{todoid}", controller.UpdateSlno).Methods("PUT") //to post
	// //delete
	router.HandleFunc("/todo/{todoid}", controller.DeleteSlno).Methods("DELETE") //to delete the data
	// // GET ALL THE list
	router.HandleFunc("/todos", controller.GetAllTodos).Methods("GET") //print all students




	// to serve static file...to run the html 
	fhandler := http.FileServer(http.Dir("./view"))
	router.PathPrefix("/").Handler(fhandler)

		log.Println("Application running on port 2023....") //just to print somemess

	err := http.ListenAndServe(":2023", router)
		if err != nil{
			os.Exit(1)
		}
}
