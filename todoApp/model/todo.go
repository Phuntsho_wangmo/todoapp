package model

import (
	"myapp/datastore/postgres"
	"myapp/utils/httpResp/date"
)


type Todo struct {
	TodoId int `json:"todoid"`
	TodoList string `json:"todolist"`
	StartDate string `json:"startdate"`
	EndDate string `json:"enddate"`
}

const (
	queryInsert     = "INSERT INTO todo(todolist, startdate, enddate) VALUES($1, $2, $3);"
	queryGetTodo    = "SELECT todoid, todolist, startdate, enddate FROM todo WHERE todoid=$1"
	queryUpdate 	= "UPDATE todo SET todoid=$1, todolist=$2, startdate=$3, enddate=$4 WHERE todoid=$1 RETURNING todoid"
	queryDeleteTodo = "DELETE FROM todo WHERE todoid=$1;"
)

// create/add
func (t *Todo) Create() error {
	var formattedDateTime = date.GetDate()
	_, err := postgres.Db.Exec(queryInsert, t.TodoList, formattedDateTime, t.EndDate)
	return err
}

// // read//get
func (t *Todo) Read() error {
	return postgres.Db.QueryRow(queryGetTodo,t.TodoId).Scan(&t.TodoId, &t.TodoList, &t.StartDate, &t.EndDate)
}

// update//put
func (t *Todo) Update(old_slno int64) error {
	err := postgres.Db.QueryRow(queryUpdate, &t.TodoId, &t.TodoList, &t.StartDate, &t.EndDate).Scan(&t.TodoId)
	return err
}

// delete
func (t *Todo) Delete() error {
	if _, err := postgres.Db.Exec(queryDeleteTodo, t.TodoId); err != nil {
		return err
	}
	return nil
}

// get all lists
func GetAllTodos() ([]Todo, error) {
	rows, err := postgres.Db.Query("SELECT * FROM todo;")
	if err != nil {
		return nil, err
	}
	//create a slice of type todo
	todos := []Todo{}
	for rows.Next() { // iterates rows one by one
		var t Todo
		dbErr := rows.Scan(&t.TodoId, &t.TodoList, &t.StartDate, &t.EndDate) //read
		if dbErr != nil {
			return nil, dbErr
		}
		todos = append(todos, t)
	}
	rows.Close()
	return todos, nil
}
