CREATE TABLE todo (
    Todoid serial PRIMARY KEY,
    TodoList varchar(50) NOT NULL,
    StartDate DATE NOT NULL,
    EndDate DATE NOT NULL
);
