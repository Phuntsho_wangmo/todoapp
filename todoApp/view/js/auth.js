// Function to check if a cookie is present
function checkCookie() {
var cookieName = "my-cookie"; // Replace with the actual name of your session cookie
var cookieValue = getCookie(cookieName);

// If cookie value is not present or empty, redirect to the login page
if (!cookieValue || cookieValue === "") {
    alert("Session time out! please login again")
    window.location.href = "index.html"; // Replace "/login" with the URL of your login page
}
}

// Function to retrieve the value of a cookie
function getCookie(name) {
var value = "; " + document.cookie;
var parts = value.split("; " + name + "=");
if (parts.length === 2) {
    return parts.pop().split(";").shift();
}
return "";
}

// Call the checkCookie function on page load
checkCookie();  