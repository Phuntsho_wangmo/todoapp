window.onload = function() {
  // Fetch todos from the server
  fetch("/todos")
    .then(response => response.json())
    .then(data => {
      console.log(data);
      showTodos(data);
    })
    .catch(error => {
      console.error('Error fetching todos:', error);
    });
};

function addTodo() {
  var data = {
    todolist: document.getElementById("todo").value,
    startdate: "",
    enddate: document.getElementById("date_time").value,
  };

  var todo = data.todolist;
  var enddate = data.enddate;

  if (todo == "" || enddate == "") {
    alert("Please fill in all fields");
    return;
  }

  fetch("/todo", {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then(response => {
      if (response.status === 201) {
        location.reload()
        return response.text();
        
      } else {
        throw new Error(response.statusText);
      }
    })
    .then(data => {
      showTodo(data);  
      resetForm();

    })
    .catch(e => {
      console.log(e); 
    });
}



function resetForm() {
  // Code to reset the form
  document.getElementById("todo").value = "";
  document.getElementById("date_time").value = "";
}

function showTodo(data) {
  const todo = JSON.parse(data);
  showTable(todo);
}

function showTodos(data) {
    data.forEach(todos => {
      showTable(todos);
    });
  }



function showTable(todo) {
  // Add a new row to the todo table
  var table = document.getElementById("myTable");
  var row = table.insertRow(table.length);
  var td = [];

  td[0] = row.insertCell(0);
  td[1] = row.insertCell(1);
  td[2] = row.insertCell(2);
  td[3] = row.insertCell(3);
  td[4] = row.insertCell(4);

  td[0].innerHTML = todo.todoid;
  td[1].innerHTML = todo.todolist;
  td[2].innerHTML = todo.startdate.split("T")[0];
  td[3].innerHTML = todo.enddate.split("T")[0];
  td[4].innerHTML = '<input type="button" onclick="deleteTodo(this)" value="Done" id="button-1">';
}

function deleteTodo(r) {
  if (confirm("Well done. Completed one todo")) {
    selectedRow = r.parentElement.parentElement;
    todoid = selectedRow.cells[0].innerHTML;

    fetch("/todo/" + todoid, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    })
    // alert("Well done. Completed one todo")
    location.reload()
  }
}
