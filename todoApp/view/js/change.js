var loggedInUser;

document.addEventListener('DOMContentLoaded', function() {
    fetch('/admins')
      .then(response => response.json())
      .then(data => {
        GetUser(data);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  });
  
  function GetUser(data) {
    try {
      const users = data;
      const usersession = JSON.parse(sessionStorage.getItem('user'));
      var name = document.getElementById('username');
      var email = document.getElementById('email');
      var password = document.getElementById('password');
      if (usersession && usersession.Email) {
        loggedInUser = users.find(user => user.Email === usersession.Email);
        if (loggedInUser) {
          name.value = loggedInUser.UserName;
          email.value = loggedInUser.Email;
          password.value = loggedInUser.Password;
        }
      }
    } catch (error) {
      console.error('Error parsing JSON:', error);
    }
  }


const update = async () => {
      var newdata = {
          username: document.getElementById('username').value,
          Email: document.getElementById('email').value,
          Password: document.getElementById('password').value
      };

      const userSession = JSON.parse(sessionStorage.getItem('user'));

    //   console.log(newdata.Name, newdata.Email, newdata.Password);

      await fetch("/signup/" + userSession.Email, {
          method: "PUT",
          body: JSON.stringify(newdata),
          headers: {"Content-type": "application/json; charset=UTF-8"}
      });

      sessionStorage.setItem('user', JSON.stringify(newdata));
      alert('credentials changed')
      window.open("todo.html", "_self")
  return false;
};
