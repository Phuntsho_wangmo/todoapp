document.addEventListener('DOMContentLoaded', function() {
  fetch('/admins')
    .then(response => response.json())
    .then(data => {
      GetUser(data);
    })
    .catch(error => {
      console.error('Error fetching data:', error);
    });
});

function GetUser(data) {
  try {
    const users = data;
    const usersession = JSON.parse(sessionStorage.getItem('user'));
    var printname = document.getElementById('printname')
    var name = document.getElementById('username');
    var email = document.getElementById('email');
    var password = document.getElementById('password');
    console.log(usersession)
    if (usersession && usersession.Email) {
      const loggedInUser = users.find(user => user.Email === usersession.Email);
      if (loggedInUser) {
        printname.innerHTML = loggedInUser.UserName
        name.textContent = loggedInUser.UserName;
        email.textContent = loggedInUser.Email;
        password.textContent = loggedInUser.Password;
      }
      sessionStorage.setItem('user',JSON.stringify(loggedInUser))
    }
  } catch (error) {
    console.error('Error parsing JSON:', error);
  }
}