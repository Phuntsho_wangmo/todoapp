function logout() {
    fetch("/logout")
    .then(response => {
        if (response.ok){
            document.cookie = 'userInfo = '+ ''
            document.cookie = "my-cookie = "+ ""
            window.open("index.html", "_self")
        }
        else{
            throw new Error(response.statusText)
        }
    })
    .catch (e => {
        alert(e)
    })
}




function confirmDeleteUser() {
    const confirmation = confirm("Are you sure you want to deactivate your account?");
    if (confirmation) {
      deleteUser();
    }
  }
  
  function deleteUser() {
    const userSession = JSON.parse(sessionStorage.getItem('user'));
    if (userSession && userSession.Email) {
      const email = userSession.Email;
      fetch(`/signup/${email}`, {
        method: 'DELETE'
      })
        .then(response => {
          if (response.ok) {
            alert('User deleted successfully');
            // Redirect to index.html
            window.location.href = 'index.html';
          } else {
            console.error('Failed to delete user');
            // Handle the error condition and display an appropriate message
          }
        })
        .catch(error => {
          console.error('An error occurred:', error);
          // Handle any network or other errors
        });
    } else {
      console.error('User session not found');
      // Handle the case when the user session is not found or invalid
    }
  }
  
  