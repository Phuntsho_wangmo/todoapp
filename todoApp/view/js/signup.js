

function signUp(){

    var _data ={
        username: document.getElementById("username").value,
        email: document.getElementById("email").value,
        password: document.getElementById("pw").value,
        pw: document.getElementById("pw1").value,
    }

        // Check if any field is empty
    if (!_data.username || !_data.email || !_data.password || !_data.pw) {
        alert("All fields are mandatory!");
        return;
    }

    if (!validateEmail(_data.email)) {
        alert("Invalid email address!");
        return;
      }

    if (_data.password !== _data.pw){
        alert("PASSWORD doesn't match!")
        return
    }

    //post because adding data
    fetch("/signup",{
        method: "POST",
        body: JSON.stringify(_data),
        headers: {"Content-type": "application/json; charset=UTF-8"}
    })
    .then(response => {
        if (response.status == 201){  //201 is success created
            // console.log("loged in")
            window.open("index.html", "_self")
            // console.log(JSON.parse(_data))
        }
    });
  }

  
function validateEmail(email) {
    // Regular expression pattern for email validation
    var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  
    // Test the email against the pattern
    return emailPattern.test(email);
  }