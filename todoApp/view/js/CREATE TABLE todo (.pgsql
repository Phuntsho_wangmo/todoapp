CREATE TABLE todo (
    Todoid serial PRIMARY KEY,
    TodoList varchar(50) NOT NULL,
    StartDate INT NOT NULL,
    EndDate INT NOT NULL
);
