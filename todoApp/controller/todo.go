package controller

import (
	"database/sql"
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"myapp/utils/httpResp/date"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// add
func AddTodoList(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "from add student handler")
	var todo model.Todo
	
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&todo)
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, "invalid json data")
		return
	}
	todo.StartDate = date.GetDate()
    defer r.Body.Close()
    

	dbErr := todo.Create()// error from the model
	if dbErr != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, dbErr.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusCreated, map[string]string{"message":"todo data added"})

	}


// // read/get
func GetTodolist(w http.ResponseWriter, r *http.Request) {
	//get url parameter
	slno := mux.Vars(r)["todoid"]
	todoid, slErr := getSlno(slno)

	// t := model.Todo{TodoList: slno}
	// fmt.Println(stud)
	if slErr != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, slErr.Error())
		return
	}
	t := model.Todo{TodoId: int(todoid)}
	getErr := t.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.ResponseWithError(w, http.StatusNotFound, "list not found")
		default:
			httpResp.ResponseWithError(w, http.StatusInternalServerError, getErr.Error())
		}
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, t) //statuscreated
}

// // convert string sid to int
func getSlno(slnoParam string) (int64, error) {
	SlNo, userErr := strconv.ParseInt(slnoParam, 10, 64)
	if userErr != nil {
		return 0, userErr
	}
	return SlNo, nil
}

// update
func UpdateSlno(w http.ResponseWriter, r *http.Request) {
	old_slno := mux.Vars(r)["todoid"] //get from URL
	old_Slno, _ := getSlno(old_slno)

	var to model.Todo
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&to)
	if err != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, "Invalid json")
		return
	}

	updateErr := to.Update(old_Slno)
	if updateErr != nil {
		switch updateErr {
		case sql.ErrNoRows:
			httpResp.ResponseWithError(w, http.StatusNotFound, "list not found")
		default:
			httpResp.ResponseWithError(w, http.StatusInternalServerError, updateErr.Error())
		}
		httpResp.ResponseWithJson(w, http.StatusOK, to)

	}
}

// // delete
func DeleteSlno(w http.ResponseWriter, r *http.Request) {
	slno := mux.Vars(r)["todoid"]
	todoid, idErr := getSlno(slno)
	if idErr != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, idErr.Error())
		return
	}
	t := model.Todo{TodoId: int(todoid)}
	if err := t.Delete(); err != nil {
		httpResp.ResponseWithJson(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, map[string]string{"status": "deleted"})
}

// get all students
func GetAllTodos(w http.ResponseWriter, r *http.Request) {
	todo, getErr := model.GetAllTodos()
	if getErr != nil {
		httpResp.ResponseWithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.ResponseWithJson(w, http.StatusOK, todo)
}

