package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAdminLoginExit(t *testing.T){
	// get url 
	url := "http://localhost:2023/login"
	var data = []byte(`{"email": "sharma@gmail.com", "password": "sharma"}`)
// caete request object
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")
	//create client to test
	client := &http.Client{}
	// do returns response 
	// send post rewuest using do function
	// request + response
	resp, err:= client.Do(req)
	if err!=nil{
		panic((err))// panic will terminate thecode
	}
	defer resp.Body.Close()
	// defer will always end at the end /last code
	//close defers until the code terminates

	body, _ := io.ReadAll(resp.Body)
	//asert is only available in testing
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	/// JSONEq = is ti check ifthe rwo json data is equal or not {}=body
	assert.JSONEq(t, `{"message":"success"}`, string(body))
}


func TestAdminLoginNoExit(t *testing.T){
	// get url 
	url := "http://localhost:2023/login"
	var data = []byte(`{"email": "pen@gmail.com", "password": "penjor"}`)
// caete request object
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")
	//create client to test
	client := &http.Client{}
	// do returns response 
	// send post rewuest using do function
	// request + response
	resp, err:= client.Do(req)
	if err!=nil{
		panic((err))// panic will terminate thecode
	}
	defer resp.Body.Close()
	// defer will always end at the end /last code
	//close defers until the code terminates

	body, _ := io.ReadAll(resp.Body)
	//asert is only available in testing
	assert.Equal(t, http.StatusUnauthorized, resp.StatusCode)
	/// JSONEq = is ti check ifthe rwo json data is equal or not {}=body
	assert.JSONEq(t, `{"error":"sql: no rows in result set"}`, string(body))
}
