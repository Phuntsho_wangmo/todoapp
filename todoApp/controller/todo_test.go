package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddTodo(t *testing.T){

	url := "http://localhost:2023/todo"
	var data = []byte(`{ "todolist": "Go project presentation", "enddate":"2023-06-13"`)
// caete request object
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")
	//create client to test
	client := &http.Client{}
	// do returns response 
	// send post rewuest using do function
	// request + response
	resp, err:= client.Do(req)
	if err!=nil{
		panic((err))// panic will terminate thecode
	}
	defer resp.Body.Close()
	// defer will always end at the end /last code
	//close defers until the code terminates

	body, _ := io.ReadAll(resp.Body)
	//asert is only available in testing
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	/// JSONEq = is ti check ifthe rwo json data is equal or not {}=body
	assert.JSONEq(t, `{"message":"Student data added"}`, string(body))
}

func TestStudentNotFound(t *testing.T){
	assert := assert.New(t)
	c:= http.Client{}
	r, _ := c.Get("http://localhost:2023/todo/70")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp := `{"error": "list not found"}`
	assert.JSONEq(expResp,string(body))
}
//delete student
func TestDeleteStudent(t *testing.T) {
	url := "http://localhost:8080/todo/2023"
	req, _ := http.NewRequest("DELETE", url, nil)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
	panic(err)
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	 
	assert.JSONEq(t, `{"status": "deleted"}`, string(body))
	}